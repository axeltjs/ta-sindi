<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dosen_kelas extends Model
{
    protected $table = 'dosen_kelas';
    protected $fillable = [
        'dosen_mk_id',
        'kelas_id',
];
}
