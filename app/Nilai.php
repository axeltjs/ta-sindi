<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nilai extends Model
{
    protected $table = 'nilai';
    protected $fillable = [
        'quiz1',
        'quiz2',
        'uts',
        'uas',
        'semester',
        'tahun',
        'dosen_mk_id',
        'mahasiswa_id',
];

    public function dosenMatkul()
    {
        return $this->belongsTo(DosenMk::class, 'dosen_mk_id');
    }

    public function mahasiswa()
    {
        return $this->belongsTo(Mahasiswa::class, 'mahasiswa_id');
    }
}
