<?php

namespace App\Http\Controllers;

use Session;

trait TraitMessage
{
    public function message($status = null)
    {
        if ($status == null) {
            $message = 'perbarui';
        } else {
            $message = 'hapus';
        }

        return Session::flash('flash_notification', [
            'level' => 'success',
            'message' => 'Data berhasil di '.$message.'!',
        ]);
    }
}
