<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nilai;
use App\Dosen;
use App\Mahasiswa;

class NilaiController extends Controller
{
    use TraitMessage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Nilai::get();

        return view('nilai.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $method = 'create';
        $dosen = Dosen::pluck('nama', 'id');
        $mahasiswa = Mahasiswa::pluck('nama', 'id');

        return view('nilai.create', compact('method', 'dosen', 'mahasiswa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        Nilai::create($data);
        $this->message();

        return redirect('nilai');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $view = [
            'method' => 'edit',
            'item' => Nilai::findOrFail($id),
            'dosen' => Dosen::pluck('nama', 'id'),
            'mahasiswa' => Mahasiswa::pluck('nama', 'id'),
        ];

        return view('nilai.create')->with($view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        Nilai::findOrFail($id)->update($data);

        $this->message();

        return redirect('nilai');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Nilai::findOrFail($id)->delete();
        $this->message(1);

        return redirect()->back();
    }

    public function rules()
    {
        $rules=[
        'quiz1'              =>'required',
        'quiz2'              =>'required',
        'uts'                =>'required',
        'uas'                =>'required',
        'semester'           =>'required',
        'tahun'              =>'required',
        'dosen_mk_id'        =>'required',
        'mahasiswa_id'       =>'required',
        ];

    }
    public function print(Request $request)
    {
        $data = Nilai::get();
        $pdf = \PDF::loadView('nilai.pdf', compact('data'))->setPaper('a4', 'portrait')
            ->setWarnings(false);

        return @$pdf->stream('nilai-print.pdf');
    }
}
