<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dosen;
use App\User;

class DosenController extends Controller
{
    use TraitMessage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Dosen::get();

        return view('dosen.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $method = 'create';
        $user = User::pluck('username', 'id');

        return view('dosen.create', compact('method', 'user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'nip' => $request->get('nip'),
            'nama' => $request->get('nama'),
            'nohp' => $request->get('nohp'),
            'user_id' => $request->get('user_id'),
        ];

        Dosen::create($data);
        $this->message();

        return redirect('dosen');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $view = [
            'method' => 'edit',
            'item' => Dosen::findOrFail($id),
            'user' => User::pluck('username', 'id'),
        ];

        return view('dosen.create')->with($view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        Dosen::findOrFail($id)->update($data);

        $this->message();

        return redirect('dosen');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Dosen::findOrFail($id)->delete();
        $this->message(1);

        return redirect()->back();
    }

public function rules()
{
    $rules=[
        'nip'       =>'required',
        'nama'      =>'required',
        'nohp'      =>'required',
        'user_id'   =>'required',
    ];
}

}
