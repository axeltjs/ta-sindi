<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DosenMk extends Model
{
    protected $table = 'dosen_mk';
    protected $fillable = [
        'dosen_id',
        'matakuliah_id',
    ];

    public function matakuliah()
    {
        return $this->belongsTo(Matakuliah::class, 'matakuliah_id');
    }
}
