<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailKehadiran extends Model
{
    protected $table = 'detail_kehadiran';
    protected $fillable = [
        'kehadiran_id',
        'status_id',
        'mahasiswa_id',
    ];
}
