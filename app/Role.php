<?php

namespace App;

use Spatie\Permission\Models\Role as Roles;

class Role extends Roles
{
    protected $table = 'roles';
    protected $fillable = ['name', 'guard_name'];
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at'];
}
