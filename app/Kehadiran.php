<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kehadiran extends Model
{
    protected $table = 'kehadiran';
    protected $fillable = [
        'tanggal',
        'jam',
        'dosen_mk_id',
    ];
}
