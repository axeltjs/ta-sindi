<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $roles = [
                [
                    'name' => 'admin',
                ], [
                    'name' => 'dosen',
                ],
            ];

        foreach ($roles as $role) {
            Role::firstOrCreate($role);
        }

        $roles = Role::get();
        foreach ($roles as $item) {
            $data = [
                    'username' => $item->name,
                    'password' => bcrypt('123123'),
                ];
            $user = User::create($data);
            $user->assignRole($item);
        }
    }
}
