<div class="form-group{{ $errors->has('nama') ? 'has-error' : '' }} ">
    {!! Form::label('nama', 'Nama', ['class'=>'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('nama', null, ['class'=>'form-control','id' => 'nama', 'autofocus', 'tabindex' => '1']) !!}
        {!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-10 col-md-offset-2">
        {!! Form::submit('Simpan', ['class'=>'btn btn-primary btn-simpan','tabindex' => '7']) !!}
    </div>
</div>