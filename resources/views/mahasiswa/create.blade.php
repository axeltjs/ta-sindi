@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            @if($method == 'create')
                <form method="post" action="{{ url('mahasiswa/') }}">
            @else
                {!! Form::model($item, ['url' => [url('mahasiswa')."/".$item->id], 'method' => 'Put']) !!}
            @endif
                <div class="card">
                    <div class="card-block">
                        <div class="Card-title-block">
                            <h3 class="title"> Data Mahasiswa </h3>
                        </div>
                        <section class="example">
                        <hr>
                            <div class="panel panel-default">
                                <div class="panel-body form-horizontal tasi-form" id="form-utama">
                                    {{csrf_field()}}
                                    @include('mahasiswa._form')
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>  
    
@endsection