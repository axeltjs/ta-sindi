<div class="form-group{{ $errors->has('nim') ? 'has-error' : '' }} ">
    {!! Form::label('nim', 'Nim', ['class'=>'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('nim', null, ['class'=>'form-control','id' => 'nim', 'autofocus', 'tabindex' => '1']) !!}
        {!! $errors->first('nim', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('nama') ? 'has-error' : '' }} ">
    {!! Form::label('nama', 'Nama', ['class'=>'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('nama', null, ['class'=>'form-control','id' => 'nama', 'autofocus', 'tabindex' => '1']) !!}
        {!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('jk') ? 'has-error' : '' }} ">
    {!! Form::label('jk', 'JenisKelamin', ['class'=>'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('jk', null, ['class'=>'form-control','id' => 'jk', 'autofocus', 'tabindex' => '1']) !!}
        {!! $errors->first('jk', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('kelas_id') ? 'has-error' : '' }} ">
    {!! Form::label('kelas_id', 'Kelas', ['class'=>'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::select('kelas_id', $kelas, null, ['class'=>'form-control','id' => 'kelas_id', 'autofocus', 'tabindex' => '1']) !!}
        {!! $errors->first('kelas_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-10 col-md-offset-2">
        {!! Form::submit('Simpan', ['class'=>'btn btn-primary btn-simpan','tabindex' => '7']) !!}
    </div>
</div>