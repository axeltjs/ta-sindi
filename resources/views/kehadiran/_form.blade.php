<div class="form-group{{ $errors->has('tanggal') ? 'has-error' : '' }} ">
    {!! Form::label('tanggal', 'Tanggal', ['class'=>'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::date('tanggal', null, ['class'=>'form-control','id' => 'tanggal', 'autofocus', 'tabindex' => '1']) !!}
        {!! $errors->first('tanggal', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('jam') ? 'has-error' : '' }} ">
    {!! Form::label('jam', 'Jam', ['class'=>'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::number('jam', null, ['class'=>'form-control','id' => 'jam', 'autofocus', 'tabindex' => '1']) !!}
        {!! $errors->first('jam', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group{{ $errors->has('dosen_mk_id') ? 'has-error' : '' }} ">
    {!! Form::label('dosen_mk_id', 'Dosen', ['class'=>'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::select('dosen_mk_id', $dosen, null, ['class'=>'form-control','id' => 'dosen_mk_id', 'autofocus', 'tabindex' => '1']) !!}
        {!! $errors->first('dosen_mk_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('mahasiswa_id') ? 'has-error' : '' }} ">
    {!! Form::label('mahasiswa_id', 'Mahasiswa', ['class'=>'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::select('mahasiswa_id', $mahasiswa, null, ['class'=>'form-control','id' => 'mahasiswa_id', 'autofocus', 'tabindex' => '1']) !!}
        {!! $errors->first('mahasiswa_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('status_id') ? 'has-error' : '' }} ">
        {!! Form::label('status_id', 'Status', ['class'=>'col-md-2 control-label']) !!}
        <div class="col-md-10">
            @foreach ($status as $item => $key)
                {{ Form::radio('status_id', $item, null, ['id' => $item]) }}
                <label for="{{ $item }}"> {{ $key }} </label>
                &nbsp;
                &nbsp;
            @endforeach
            {!! $errors->first('status_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    
    
<div class="form-group">
    <div class="col-md-10 col-md-offset-2">
        {!! Form::submit('Simpan', ['class'=>'btn btn-primary btn-simpan','tabindex' => '7']) !!}
    </div>
</div>