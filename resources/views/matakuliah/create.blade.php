@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            @if($method == 'create')
                <form method="post" action="{{ url('matakuliah/') }}">
            @else
                {!! Form::model($item, ['url' => [url('matakuliah')."/".$item->id], 'method' => 'Put']) !!}
            @endif
                <div class="card">
                    <div class="card-block">
                        <div class="Card-title-block">
                            <h3 class="title"> Input Data MataKuliah </h3>
                        </div>
                        <section class="example">
                        <hr>
                            <div class="panel panel-default">
                                <div class="panel-body form-horizontal tasi-form" id="form-utama">
                                    {{csrf_field()}}
                                    @include('matakuliah._form')
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>  
    
@endsection