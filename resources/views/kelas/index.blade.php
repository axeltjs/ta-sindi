@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Kelas
                <div class="card-body">
                    <a href="{{ url('kelas/create')}}" class="btn btn-primary">Tambah Data</a>
                    <br><br>
                    <table class="table table-bordered">
                        <tr>
                            <td>No</td>
                            <td>Nama</td>
                            <td>Angkatan</td>
                            <td>Aksi</td>
                        </tr>
                        @foreach($data as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->nama }}</td>
                                <td>{{ $item->angkatan }}</td>
                                <td>
                                    <a href="{{ url('kelas/'.$item->id.'/edit') }}" class="btn btn-block btn-warning">Edit</a>
                                    <form action="{{ url('/kelas', ['id' => $item->id]) }}" method="post">
                                        <input class="btn btn-danger btn-block" type="submit" value="Hapus" />
                                        @method('delete')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
