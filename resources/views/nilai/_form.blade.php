<div class="form-group{{ $errors->has('quiz1') ? 'has-error' : '' }} ">
    {!! Form::label('quiz1', 'Quiz1', ['class'=>'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::number('quiz1', null, ['class'=>'form-control','id' => 'quiz1', 'autofocus', 'tabindex' => '1']) !!}
        {!! $errors->first('quiz1', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('Quiz2') ? 'has-error' : '' }} ">
    {!! Form::label('Quiz2', 'Quiz2', ['class'=>'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::number('quiz2', null, ['class'=>'form-control','id' => 'Quiz2', 'autofocus', 'tabindex' => '1']) !!}
        {!! $errors->first('Quiz2', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('uts') ? 'has-error' : '' }} ">
    {!! Form::label('uts', 'Uts', ['class'=>'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::number('uts', null, ['class'=>'form-control','id' => 'uts', 'autofocus', 'tabindex' => '1']) !!}
        {!! $errors->first('uts', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('uas') ? 'has-error' : '' }} ">
    {!! Form::label('uas', 'Uas', ['class'=>'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::number('uas', null, ['class'=>'form-control','id' => 'uas', 'autofocus', 'tabindex' => '1']) !!}
        {!! $errors->first('uas', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('semester') ? 'has-error' : '' }} ">
    {!! Form::label('semester', 'Semester', ['class'=>'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::number('semester', null, ['class'=>'form-control','id' => 'semester', 'autofocus', 'tabindex' => '1']) !!}
        {!! $errors->first('semester', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('tahun') ? 'has-error' : '' }} ">
    {!! Form::label('tahun', 'Tahun', ['class'=>'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::number('tahun', null, ['class'=>'form-control','id' => 'tahun', 'autofocus', 'tabindex' => '1']) !!}
        {!! $errors->first('tahun', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group{{ $errors->has('dosen_mk_id') ? 'has-error' : '' }} ">
    {!! Form::label('dosen_mk_id', 'Dosen', ['class'=>'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::select('dosen_mk_id', $dosen, null, ['class'=>'form-control','id' => 'dosen_mk_id', 'autofocus', 'tabindex' => '1']) !!}
        {!! $errors->first('dosen_mk_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('mahasiswa_id') ? 'has-error' : '' }} ">
    {!! Form::label('mahasiswa_id', 'Mahasiswa', ['class'=>'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::select('mahasiswa_id', $mahasiswa, null, ['class'=>'form-control','id' => 'mahasiswa_id', 'autofocus', 'tabindex' => '1']) !!}
        {!! $errors->first('mahasiswa_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-10 col-md-offset-2">
        {!! Form::submit('Simpan', ['class'=>'btn btn-primary btn-simpan','tabindex' => '7']) !!}
    </div>
</div>