
<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <table>
                            <tr>
                                <td>NIM</td>
                                <td>:</td>
                                <td>{{ $mhs->nim }}</td>
                            </tr>
                            <tr>
                                <td>Nama Mahasiswa</td>
                                <td>:</td>
                                <td>{{ $mhs->nama }}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="card-body">
                        <table width="100%" class="table table-bordered" border="1">
                            <tr>
                                <td width="1%">No</td>
                                <td>Matakuliah</td>
                                <td>Nilai</td>
                            </tr>
                            @php $sub_total = 0; $matkul = 0; @endif
                            @foreach($data as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->dosenMatkul ? $item->dosenMatkul->matakuliah->nama : '-' }}</td>
                                    <td>
                                        @php
                                            $q1 = $item->quiz1 * 0.1;
                                            $q2 = $item->quiz2 * 0.1;
                                            $uts = $item->uts * 0.35;
                                            $uas = $item->uas * 0.40;
                                            $kehadiran = 5;
                                            $total = ($q1 + $q2 + $uts + $uas + $kehadiran);
                                            
                                            $sub_total += $total;
                                            $matkul++;
                                        @endphp
                                        {{ $total }}
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="3">IPK</td>
                                <td>
                                    @php
                                        $kumulatih = $sub_total/$total*100;
                                        
                                    @endphp
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    