@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Nilai Mahasiswa
                <div class="card-body">
                    <a href="{{ url('nilai/create')}}" class="btn btn-primary">Tambah Data</a>
                    <a href="{{ route('nilai.pdf.cetak')}}" target="__blank" class="pull-right btn btn-success">Cetak Data</a>
                    <br><br>
                    <table class="table table-bordered">
                        <tr>
                            <td>No</td>
                            <td>Quiz 1</td>
                            <td>Quiz 2</td>
                            <td>Uts</td>
                            <td>Uas</td>
                            <td>Semester</td>
                            <td>Tahun</td>
                            <td>Aksi</td>
                        </tr>
                        @foreach($data as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->quiz1 }}</td>
                                <td>{{ $item->quiz2 }}</td>
                                <td>{{ $item->uts }}</td>
                                <td>{{ $item->uas }}</td>
                                <td>{{ $item->semester }}</td>
                                <td>{{ $item->tahun }}</td>
                                <td>
                                    <a href="{{ url('nilai/'.$item->id.'/edit') }}" class="btn btn-block btn-warning">Edit</a>
                                    <form action="{{ url('/nilai', ['id' => $item->id]) }}" method="post">
                                        <input class="btn btn-danger btn-block" type="submit" value="Hapus" />
                                        @method('delete')
                                        @csrf
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
