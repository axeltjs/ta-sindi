<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['auth']], function () {
    Route::resource('dosen', 'DosenController');
    Route::resource('kelas', 'KelasController');
    Route::resource('matakuliah', 'MatakuliahController');
    Route::resource('mahasiswa', 'MahasiswaController');
    Route::resource('kehadiran', 'KehadiranController');
    Route::resource('status', 'StatusController');
    Route::resource('nilai', 'NilaiController');
    Route::get('nilai/pdf/cetak', 'NilaiController@print')->name('nilai.pdf.cetak');
});
